#!/bin/bash
GPU=0
ID=$(hostname)-${GPU//,/}-${USER//[^a-zA-Z0-9]/-}
IMAGE=gpu-registry:5000/yoshimasa.machida/vrp-rl-cuda9
MEMORY=16g

docker run \
  --detach \
  --env NVIDIA_VISIBLE_DEVICES=$GPU \
  --hostname=$ID \
  --memory=$MEMORY \
  --name=$ID \
  --publish-all \
  --rm \
  --volume /home:/home \
  $IMAGE \

PORT=$(docker port $ID | grep 22/tcp | awk -F: '{print $2}')
echo ssh $USER@$(hostname) -p $PORT
