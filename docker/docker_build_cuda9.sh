#!/bin/bash

IMAGE=vrp-rl-cuda9
VERSION=latest

docker build -t gpu-registry:5000/$USER/$IMAGE:$VERSION -f ./Dockerfile.bp.cuda9 .
