# README

## 概要

運搬経路問題を強化学習で解くために、問題設定やアルゴリズムの調査を行う。

[Reinforcement Learning for Solving the Vehicle Routing Problem](https://arxiv.org/abs/1802.04240)という論文が投稿されており、また[その実装が公開](https://github.com/OptMLGroup/VRP-RL)されているため、論文の理解と同時にコードの動作を確認する。

## プロジェクト

- [工程間搬送最適化プロジェクト（YKK工機技術本部）](https://brainpad.atlassian.net/wiki/spaces/YKK/pages/895844683)

## Clone

- gitのsubmoduleを利用しているため、このレポジトリをcloneする際には`--recursive`オプションが必要である

```bash
git clone --recursive https://your-username@bitbucket.org/bp-yoshimasa-machida/vrp-rl-sample.git
```

- もし`--recursive`オプションをつけ忘れた場合は、clone済みのディレクトリで下記コマンドを入力する

```bash
git submodule init
git submodule update
```


## 環境構築

- GPGPU環境を想定している
- GPGPUの対応CUDAによってビルドするDockerfileが異なる
  - https://brainpad.atlassian.net/wiki/spaces/ANALYSIS/pages/810222895
- このdocker containerでは新規に`python3.6`コマンドをインストールしている
- `python`コマンドを入力する際には、すべて`python3.6`に置き換えて実行する必要がある

```bash
cd vrp-rl-sample
cd docker

# build docker image
# if cuda 9
bash docker_build_cuda9.sh
# if cuda 10
bash docker_build_cuda10.sh

# run docker container
# if cuda 9
bash docker_run_cuda9.sh
# if cuda 10
bash docker_run_cuda10.sh
```

## サンプルコードの実行

- https://github.com/OptMLGroup/VRP-RL をsubmoduleとして取り込んでいる
- サンプルコードの実行の際には、VRP-RLディレクトリに移動し`python`コマンドを`python3.6`に置き換えれば良い
